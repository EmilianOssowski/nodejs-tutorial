
const http = require('http');
const fs = require('fs');

const myReadStream = fs.createReadStream(__dirname + '/assets/lorem.txt', {encoding : 'utf8'});

let chunkCount = 0;
myReadStream.on('data', (chunk)=>{
    if(chunk) {
        chunkCount += 1;
    }
    console.log(chunkCount)
})
